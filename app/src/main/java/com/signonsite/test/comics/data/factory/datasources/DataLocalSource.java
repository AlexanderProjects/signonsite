package com.signonsite.test.comics.data.factory.datasources;

import com.signonsite.test.comics.domain.models.Comic;
import java.util.List;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

public interface DataLocalSource {

    void insertComic(Comic comic);

    void saveTheLastComicId();

    void deleteComic(Comic comic);

    void updateComic(Comic comic);

    Flowable<List<Comic>> obtainListOfComics();

    Maybe<String> getLastComicId();

    Maybe<Integer> QuantityOfDataInDb();

}
