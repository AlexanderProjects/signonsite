package com.signonsite.test.comics.di.modules;

import com.signonsite.test.comics.ui.home.ComicsMainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract ComicsMainActivity contributeComicsMainActivity();
}
