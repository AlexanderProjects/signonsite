package com.signonsite.test.comics.ui.home;

import android.os.Bundle;
import android.os.StrictMode;

import androidx.fragment.app.FragmentManager;

import com.signonsite.test.comics.R;
import com.signonsite.test.comics.ui.notifications.Notifier;

import dagger.android.support.DaggerAppCompatActivity;


public class ComicsMainActivity extends DaggerAppCompatActivity{

    HomeComicsListFragment homeFragment = HomeComicsListFragment.newInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Notifier notifier = new Notifier(this);
        notifier.execute();

        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.container, homeFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, homeFragment)
                .addToBackStack(null)
                .commit();
    }
}
