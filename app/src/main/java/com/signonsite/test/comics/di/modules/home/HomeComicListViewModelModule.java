package com.signonsite.test.comics.di.modules.home;

import androidx.lifecycle.ViewModel;

import com.signonsite.test.comics.di.scope.ViewModelKey;
import com.signonsite.test.comics.ui.home.HomeComicsListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class HomeComicListViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeComicsListViewModel.class)
    public abstract ViewModel homeComicsViewModel(HomeComicsListViewModel homeViewModel);

}
