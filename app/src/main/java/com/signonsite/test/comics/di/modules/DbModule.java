package com.signonsite.test.comics.di.modules;

import com.google.firebase.firestore.FirebaseFirestore;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbModule {

        @Provides
        @Singleton
        FirebaseFirestore provideFirestore(){ return FirebaseFirestore.getInstance(); }

}
