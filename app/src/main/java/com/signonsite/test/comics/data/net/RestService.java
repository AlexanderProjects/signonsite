package com.signonsite.test.comics.data.net;

import android.graphics.drawable.Drawable;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.signonsite.test.comics.data.factory.datasources.DataNetSource;
import com.signonsite.test.comics.domain.models.Comic;

import io.reactivex.Flowable;
import retrofit2.Retrofit;

public class RestService implements DataNetSource {

    private Retrofit retrofit;
    private RequestManager requestManager;

    public RestService(Retrofit retrofit, RequestManager requestManager) {
        this.retrofit = retrofit;
        this.requestManager = requestManager;
    }

    @Override
    public Flowable<Comic> obtainComic(String comicId) {
        return retrofit.create(ComicsApi.class).getComicFromXkcd(comicId);
    }

    @Override
    public RequestBuilder<Drawable> loadImage(String imgUrl) {
        return requestManager.load(imgUrl);
    }

    @Override
    public Flowable<Comic> obtainLatestComic() {
        return retrofit.create(ComicsApi.class).getLatestComicXkcd();
    }
}
