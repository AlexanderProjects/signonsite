package com.signonsite.test.comics.di.modules.details;

import androidx.lifecycle.ViewModel;

import com.signonsite.test.comics.di.scope.ViewModelKey;
import com.signonsite.test.comics.ui.details.ComicDetailsViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ComicDetailsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ComicDetailsViewModel.class)
    public abstract ViewModel comicDetailsViewModel(ComicDetailsViewModel comicDetailsViewModel);

}



