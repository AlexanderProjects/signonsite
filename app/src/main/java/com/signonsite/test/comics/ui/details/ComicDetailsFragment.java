package com.signonsite.test.comics.ui.details;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.signonsite.test.comics.R;
import com.signonsite.test.comics.di.factory.ViewModelFactory;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.ui.home.HomeComicsListFragment;
import com.signonsite.test.comics.ui.shop.ShoppingFragment;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.DaggerFragment;

public class ComicDetailsFragment extends DaggerFragment {

    @Inject
    ViewModelFactory viewModelFactory;

    private ComicDetailsViewModel viewModel;


    public static ComicDetailsFragment newInstance() {
        return new ComicDetailsFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comic_details_fragment, container, false);
        ImageButton deleteComic = view.findViewById(R.id.delete_comic);
        ImageButton saveComic = view.findViewById(R.id.save_comic);
        ImageButton backNavigation = view.findViewById(R.id.back_nav);

        viewModel = ViewModelProviders.of(this,viewModelFactory).get(ComicDetailsViewModel.class);
        viewModel.setComicSavedState(getArguments().getParcelable("Comic"));
        viewModel.imageProcessor(viewModel.getComicSavedState().getImg()).into((ImageView) view.findViewById(R.id.comicImageView));

        deleteComic.setOnClickListener(deleteView -> {
            viewModel.deleteComic();
            Toast.makeText(getActivity(),"Comic Deleted",Toast.LENGTH_SHORT).show();
        });

        saveComic.setOnClickListener(saveView -> {
            viewModel.getComicSavedState().setSaved(true);
             viewModel.updateComic();
            Toast.makeText(getActivity(),"Comic Saved",Toast.LENGTH_SHORT).show();
        });

        backNavigation.setOnClickListener(viewNav ->
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, HomeComicsListFragment.newInstance())
                    .addToBackStack("Home")
                    .commitAllowingStateLoss());

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(!viewModel.getComicSavedState().getSaved())
            viewModel.deleteComic();
    }
}
