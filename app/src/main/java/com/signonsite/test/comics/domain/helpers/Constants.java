package com.signonsite.test.comics.domain.helpers;

public class Constants {

    public final static String baseUrl = "https://xkcd.com";
    public final static String xkcdMerchandiseUrl = "https://store.xkcd.com/";
    public final static Integer threshold = 13;
    public final static Integer batchSize = 23;
    public final static Integer InitialValue = 2140;

}
