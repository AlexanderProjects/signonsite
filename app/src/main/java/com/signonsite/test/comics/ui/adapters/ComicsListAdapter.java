package com.signonsite.test.comics.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.signonsite.test.comics.R;
import com.signonsite.test.comics.domain.models.Comic;

import java.util.List;

public class ComicsListAdapter extends RecyclerView.Adapter<ComicsListAdapter.ViewHolder>
{

    private List<Comic> comics;
    private final OnItemClickListener clickListener;
    private final String saved = "Saved";
    private final String unSaved = "UnSaved";

    public ComicsListAdapter(List<Comic> comics, OnItemClickListener clickListener) {
        this.comics = comics;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = layoutInflater.inflate(R.layout.home_comics_list_item, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(comics.get(position), position, clickListener);
    }

    @Override
    public int getItemCount() {
        return comics.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView numberPosition;
        private TextView title;
        private TextView isSaved;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            numberPosition = itemView.findViewById(R.id.number);
            title = itemView.findViewById(R.id.title);
            isSaved = itemView.findViewById(R.id.isSaved);
        }

        void bind(Comic comic, int position, final OnItemClickListener listener) {
            numberPosition.setText(String.valueOf(position + 1));
            title.setText(comic.getTitle());
            isSaved.setText(isComicSaved(comic));
            itemView.setOnClickListener(view -> listener.onItemClick(comic));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Comic item);
    }

    private String isComicSaved(Comic comic){
        if(comic.getSaved()){
            return saved;
        }else {
            return unSaved;
        }
    }

}
