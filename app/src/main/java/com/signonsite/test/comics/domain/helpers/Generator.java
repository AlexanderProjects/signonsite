package com.signonsite.test.comics.domain.helpers;

import android.util.Pair;

import com.signonsite.test.comics.domain.models.Comic;

import java.util.ArrayList;
import java.util.List;

public class Generator {

    public static String[] generateInitialComicNumbers(){

        List<String> listOfNumbers = new ArrayList<>();

        for(int i = 0; i < 23; i++){
            listOfNumbers.add(String.valueOf(i + Constants.InitialValue));
        }
        return listOfNumbers.toArray(new String[0]);
    }

    public static List<String> generateComicIdsToUpdate(Pair pairIds, int listSize){

        List<String> listOfComics = new ArrayList<>();
        if(listSize < Constants.threshold) {

            int latestComicId = Integer.parseInt((String) pairIds.first);
            int latestComicPublished = Integer.parseInt(((Comic) pairIds.second).getNum());
            int nextComicId = latestComicId + 1;
            int numberItemsToUpdate = Constants.batchSize - listSize;

            if (listSize < Constants.batchSize) {
                for (int i = 0; i < numberItemsToUpdate; i++) {
                    if (nextComicId + i < latestComicPublished)
                        listOfComics.add(String.valueOf(nextComicId + i));
                }
            }
        }
        return listOfComics;
    }
}
