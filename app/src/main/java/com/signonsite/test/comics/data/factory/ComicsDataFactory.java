package com.signonsite.test.comics.data.factory;

import com.bumptech.glide.RequestManager;
import com.google.firebase.firestore.FirebaseFirestore;
import com.signonsite.test.comics.data.factory.datasources.DataLocalSource;
import com.signonsite.test.comics.data.factory.datasources.DataNetSource;
import com.signonsite.test.comics.data.local.DatabaseService;
import com.signonsite.test.comics.data.net.RestService;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class ComicsDataFactory {

    private Retrofit retrofit;
    private FirebaseFirestore firestore;
    private RequestManager requestManager;

    @Inject
    public ComicsDataFactory(Retrofit retrofit,
                             FirebaseFirestore firestore,
                             RequestManager requestManager) {
        this.retrofit = retrofit;
        this.firestore = firestore;
        this.requestManager = requestManager;
    }

    public DataNetSource createNetSourceData () { return new RestService(retrofit, requestManager); }

    public DataLocalSource createLocalSourceData(){ return new DatabaseService(firestore); }
}
