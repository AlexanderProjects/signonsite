package com.signonsite.test.comics.ui.home;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.signonsite.test.comics.R;
import com.signonsite.test.comics.di.factory.ViewModelFactory;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.ui.adapters.ComicsListAdapter;
import com.signonsite.test.comics.ui.details.ComicDetailsFragment;
import com.signonsite.test.comics.ui.shop.ShoppingFragment;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class HomeComicsListFragment extends DaggerFragment implements HomeComicsView {

    @Inject
    ViewModelFactory viewModelFactory;

    private HomeComicsListViewModel viewModel;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView listOfComics;
    private ComicsListAdapter adapter;


    public static HomeComicsListFragment newInstance() {
        return new HomeComicsListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_comics_list_fragment, container, false);
        listOfComics = view.findViewById(R.id.list_of_comics);
        ImageButton shopping = view.findViewById(R.id.shopping);

        shopping.setOnClickListener(view1 -> {
            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, ShoppingFragment.newInstance())
                    .addToBackStack("shop")
                    .commit();
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        listOfComics.setLayoutManager(linearLayoutManager);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeComicsListViewModel.class);
        viewModel.inject(this);
        viewModel.getListOfComics();
    }

    @Override
    public void setAdapter(List<Comic> comics) {
        adapter = new ComicsListAdapter(comics, item -> {

            Bundle bundle = new Bundle();
            bundle.putParcelable("Comic",item);

            ComicDetailsFragment fragment = ComicDetailsFragment.newInstance();
            fragment.setArguments(bundle);

            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack("details")
                    .commit();
        });
        listOfComics.setAdapter(adapter);

    }

}
