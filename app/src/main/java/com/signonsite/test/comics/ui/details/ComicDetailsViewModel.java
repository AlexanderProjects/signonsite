package com.signonsite.test.comics.ui.details;

import androidx.lifecycle.ViewModel;

import com.bumptech.glide.RequestBuilder;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.domain.usecases.ComicDetailsUseCase;

import javax.inject.Inject;

public class ComicDetailsViewModel extends ViewModel {

    private ComicDetailsUseCase comicDetailsUseCase;
    private Comic comicSavedState = new Comic();

    public Comic getComicSavedState() {
        return comicSavedState;
    }

    public void setComicSavedState(Comic comicSavedState) {
        this.comicSavedState = comicSavedState;
    }

    @Inject
    public ComicDetailsViewModel(ComicDetailsUseCase comicDetailsUseCase) {
        this.comicDetailsUseCase = comicDetailsUseCase;
    }

    public RequestBuilder imageProcessor(String url){ return comicDetailsUseCase.processImage(url);}

    public void deleteComic(){ comicDetailsUseCase.deleteComic(this.comicSavedState); }

    public void updateComic(){ comicDetailsUseCase.saveComic(this.comicSavedState); }


}
