package com.signonsite.test.comics.data.net;

import com.signonsite.test.comics.domain.models.Comic;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ComicsApi {

    @GET("/{comicId}/info.0.json")
    Flowable<Comic> getComicFromXkcd(@Path("comicId")String comicId);

    @GET("/info.0.json")
    Flowable<Comic> getLatestComicXkcd();
}
