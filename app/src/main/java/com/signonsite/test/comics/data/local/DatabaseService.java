package com.signonsite.test.comics.data.local;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.signonsite.test.comics.data.factory.datasources.DataLocalSource;
import com.signonsite.test.comics.domain.models.Comic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Maybe;


public class DatabaseService implements DataLocalSource {

    private static final String collectionComicsData = "Xkcd";
    private static final String collectionComicsIdsData = "ComicsIds";
    private FirebaseFirestore firestore;

    public DatabaseService(FirebaseFirestore firestore) {
        this.firestore = firestore;
    }


    @Override
    public void insertComic(Comic comic) {
        firestore.collection(collectionComicsData).document()
                .set(comic).addOnSuccessListener(aVoid -> {
            Log.d("Data Saved :", "Successfully");
        });
    }

    @Override
    public void saveTheLastComicId() {
        CollectionReference colRef = firestore.collection(collectionComicsData);
        Query query = colRef.orderBy("num", Query.Direction.DESCENDING).limit(1);
        query.addSnapshotListener((queryDocumentSnapshots, e) -> {

            if (e != null) {
                Log.d("Database error: ", e.toString());
                return;
            }

            if (queryDocumentSnapshots != null) {
                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    Map<String, Object> comicsIds = new HashMap<>();
                    comicsIds.put("num",documentSnapshot.toObject(Comic.class).getNum());
                    firestore.collection(collectionComicsIdsData).document("latestComicId")
                            .set(comicsIds);
                }
            }
        });
    }

    @Override
    public void deleteComic(Comic comic) {
        CollectionReference colRef = firestore.collection(collectionComicsData);

        Query query = colRef.whereEqualTo("num",comic.getNum());
        query.limit(1).addSnapshotListener((queryDocumentSnapshots, e) -> {

            if (queryDocumentSnapshots != null) {
                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    firestore.collection(collectionComicsData)
                            .document(documentSnapshot.getId())
                            .delete();
                }
            }

        });
    }

    @Override
    public void updateComic(Comic comic) {
        CollectionReference colRef = firestore.collection(collectionComicsData);
        Query query = colRef.whereEqualTo("num",comic.getNum());
        query.limit(1).addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (queryDocumentSnapshots != null) {
                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    firestore.collection(collectionComicsData)
                            .document(documentSnapshot.getId())
                            .update("saved",comic.getSaved());
                }
            }

        });
    }

    @Override
    public Flowable<List<Comic>> obtainListOfComics() {
        return Flowable.create(emitter -> firestore.collection(collectionComicsData)
                        .orderBy("num", Query.Direction.ASCENDING).limit(10)
                        .addSnapshotListener((queryDocumentSnapshots, e) -> {
                            if (e != null) {
                                Log.d("Database error: ", e.toString());
                                emitter.onError(e);
                                return;
                            }
                            if (queryDocumentSnapshots != null) {
                                List<Comic> comicList = new ArrayList<>();
                                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                                    comicList.add(documentSnapshot.toObject(Comic.class));
                                }
                                Collections.reverse(comicList);
                                emitter.onNext(comicList);

                            }
                        })
        , BackpressureStrategy.BUFFER);
    }

    @SuppressLint("CheckResult")
    @Override
    public Maybe<String> getLastComicId() {
        return Maybe.create(emitter -> firestore.collection(collectionComicsData)
                        .orderBy("num", Query.Direction.ASCENDING)
                        .addSnapshotListener((queryDocumentSnapshots, e) -> {
                            DocumentReference docRef = firestore.collection(collectionComicsIdsData).document("latestComicId");
                            docRef.get().addOnSuccessListener(documentSnapshot -> {
                                if(documentSnapshot.exists()){
                                    Object document = documentSnapshot.get("num");
                                    emitter.onSuccess(String.valueOf(document));
                                }
                            });
                        }));
    }

    @Override
    public Maybe<Integer> QuantityOfDataInDb() {
        return Maybe.create(emitter ->firestore.collection(collectionComicsData).get().addOnSuccessListener(documentSnapshots -> {
                if(!documentSnapshots.isEmpty()){
                    emitter.onSuccess(documentSnapshots.size());
                }else{
                    emitter.onComplete();
                }
            }
        ));
    }

}
