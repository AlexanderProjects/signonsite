package com.signonsite.test.comics.ui.home;

import com.signonsite.test.comics.domain.models.Comic;

import java.util.List;

public interface HomeComicsView {
    void setAdapter(List<Comic> comics);
}
