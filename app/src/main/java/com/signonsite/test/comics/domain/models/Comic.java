package com.signonsite.test.comics.domain.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Comic implements Parcelable {

    private String alt;
    private String day;
    private String img;
    private String link;
    private String month;
    private String news;
    private String num;
    private String safe_title;
    private String title;
    private String transcript;
    private String year;
    private transient boolean isSaved = false;

    public Comic() { }

    protected Comic(Parcel in) {
        alt = in.readString();
        day = in.readString();
        img = in.readString();
        link = in.readString();
        month = in.readString();
        news = in.readString();
        num = in.readString();
        safe_title = in.readString();
        title = in.readString();
        transcript = in.readString();
        year = in.readString();
    }

    public static final Creator<Comic> CREATOR = new Creator<Comic>() {
        @Override
        public Comic createFromParcel(Parcel in) {
            return new Comic(in);
        }

        @Override
        public Comic[] newArray(int size) {
            return new Comic[size];
        }
    };

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getSafe_title() {
        return safe_title;
    }

    public void setSafe_title(String safe_title) {
        this.safe_title = safe_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTranscript() {
        return transcript;
    }

    public void setTranscript(String transcript) {
        this.transcript = transcript;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Boolean getSaved() {
        return isSaved;
    }

    public void setSaved(Boolean saved) {
        isSaved = saved;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(alt);
        parcel.writeString(day);
        parcel.writeString(img);
        parcel.writeString(link);
        parcel.writeString(month);
        parcel.writeString(news);
        parcel.writeString(num);
        parcel.writeString(safe_title);
        parcel.writeString(title);
        parcel.writeString(transcript);
        parcel.writeString(year);
    }
}
