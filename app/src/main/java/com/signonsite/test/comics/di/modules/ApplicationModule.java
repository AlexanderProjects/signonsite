package com.signonsite.test.comics.di.modules;

import com.bumptech.glide.RequestManager;
import com.google.firebase.firestore.FirebaseFirestore;
import com.signonsite.test.comics.data.ComicsDataRepository;
import com.signonsite.test.comics.data.factory.ComicsDataFactory;
import com.signonsite.test.comics.domain.repository.ComicsRepository;
import com.signonsite.test.comics.domain.usecases.ComicDetailsUseCase;
import com.signonsite.test.comics.domain.usecases.HomeComicsListUseCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApplicationModule {


    @Provides
    @Singleton
    ComicsDataFactory provideDataFactory(Retrofit retrofit, FirebaseFirestore firestore, RequestManager requestManager) {
        return new ComicsDataFactory(retrofit, firestore, requestManager);
    }

    @Provides
    @Singleton
    ComicsRepository provideRepository(ComicsDataFactory factory){ return new ComicsDataRepository(factory); }

    @Provides
    @Singleton
    HomeComicsListUseCase provideHomeComicsListUseCase(ComicsRepository repository) { return new HomeComicsListUseCase(repository);}

    @Provides
    @Singleton
    ComicDetailsUseCase provideComicDetailsUseCase(ComicsRepository repository){ return new ComicDetailsUseCase(repository);}

}
