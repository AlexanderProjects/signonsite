package com.signonsite.test.comics.di.components;



import com.signonsite.test.comics.ComicsApplication;
import com.signonsite.test.comics.di.modules.ActivityBuilderModule;
import com.signonsite.test.comics.di.modules.ApplicationModule;
import com.signonsite.test.comics.di.modules.DbModule;
import com.signonsite.test.comics.di.modules.FragmentBuilderModule;
import com.signonsite.test.comics.di.modules.NetModule;
import com.signonsite.test.comics.di.modules.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,
        ActivityBuilderModule.class,
        FragmentBuilderModule.class,
        ApplicationModule.class,
        ViewModelModule.class,
        NetModule.class,
        DbModule.class})
public interface ApplicationComponent extends AndroidInjector<ComicsApplication> {


    @Component.Builder
    interface Builder{

        @BindsInstance
        Builder application(ComicsApplication application);

        ApplicationComponent build();
    }

}
