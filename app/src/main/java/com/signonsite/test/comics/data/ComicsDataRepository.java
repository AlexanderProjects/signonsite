package com.signonsite.test.comics.data;

import com.bumptech.glide.RequestBuilder;
import com.signonsite.test.comics.data.factory.ComicsDataFactory;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.domain.repository.ComicsRepository;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Flowable;
import io.reactivex.Maybe;


public class ComicsDataRepository implements ComicsRepository {

    private ComicsDataFactory factory;

    @Inject
    public ComicsDataRepository(ComicsDataFactory factory) {
        this.factory = factory;
    }

    @Override
    public void insertComic(Comic comic) {
         factory.createLocalSourceData().insertComic(comic);
    }

    @Override
    public void saveTheLastComicId() {
        factory.createLocalSourceData().saveTheLastComicId();
    }

    @Override
    public void deleteComic(Comic comic) {
        factory.createLocalSourceData().deleteComic(comic);
    }

    @Override
    public void updateComic(Comic comic) {
        factory.createLocalSourceData().updateComic(comic);
    }

    @Override
    public Flowable<Comic> getComic(String comicId) {
        return factory.createNetSourceData().obtainComic(comicId);
    }

    @Override
    public Flowable<List<Comic>> getListComics() {
        return factory.createLocalSourceData().obtainListOfComics();
    }

    @Override
    public Maybe<String> getLastComicId() {
        return factory.createLocalSourceData().getLastComicId();
    }

    @Override
    public Flowable<Comic> getLastComicIdPublished() {
        return factory.createNetSourceData().obtainLatestComic();
    }

    @Override
    public Maybe<Integer> CheckNumberOfRecordsInDb() {
        return factory.createLocalSourceData().QuantityOfDataInDb();
    }


    @Override
    public RequestBuilder getImage(String imgUrl) {
        return factory.createNetSourceData().loadImage(imgUrl);
    }
}
