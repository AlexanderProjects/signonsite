package com.signonsite.test.comics.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.signonsite.test.comics.di.factory.ViewModelFactory;
import com.signonsite.test.comics.di.scope.ViewModelKey;
import com.signonsite.test.comics.ui.home.HomeComicsListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory viewModelFactory);

}