package com.signonsite.test.comics.data.factory.datasources;

import com.bumptech.glide.RequestBuilder;
import com.signonsite.test.comics.domain.models.Comic;
import io.reactivex.Flowable;


public interface DataNetSource {

    Flowable<Comic> obtainComic(String comicId);

    Flowable<Comic> obtainLatestComic();

    RequestBuilder loadImage(String imgUrl);

}
