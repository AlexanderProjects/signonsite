package com.signonsite.test.comics.domain.usecases;

import android.annotation.SuppressLint;
import android.util.Log;
import android.util.Pair;

import com.signonsite.test.comics.domain.helpers.Constants;
import com.signonsite.test.comics.domain.helpers.Generator;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.domain.repository.ComicsRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class HomeComicsListUseCase {

    private ComicsRepository comicsRepository;

    @Inject
    public HomeComicsListUseCase(ComicsRepository comicsRepository) {
        this.comicsRepository = comicsRepository;
    }

    @SuppressLint("CheckResult")
    public Flowable<Object> getInitialListOfComics(){
        Flowable<String> comicsFlowable = Flowable.fromArray(Generator.generateInitialComicNumbers());
        comicsFlowable
                .flatMap(s -> comicsRepository.getComic(s))
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableSubscriber<Comic>() {
                    @Override
                    public void onNext(Comic comic) { comicsRepository.insertComic(comic); }
                    @Override
                    public void onError(Throwable t) {
                        Log.d("Error", t.getMessage());
                    }
                    @Override
                    public void onComplete() {
                        Log.d("Complete", "Complete");
                        comicsRepository.saveTheLastComicId();
                    }
                });

        return Flowable.concat(comicsFlowable, comicsRepository.getListComics().subscribeOn(Schedulers.io()));

    }

    @SuppressLint("CheckResult")
    public void updateDataBase(int listSize){
            comicsRepository.getLastComicId()
                    .subscribe(id -> comicsRepository.getLastComicIdPublished()
                            .flatMap(comic ->  Flowable.just(new Pair<>(id, comic)))
                            .flatMap(pair -> Flowable.fromArray(Generator.generateComicIdsToUpdate(pair,listSize).toArray()))
                            .flatMap(s -> comicsRepository.getComic((String) s))
                            .subscribeOn(Schedulers.io())
                            .subscribeWith(new DisposableSubscriber<Comic>() {
                                @Override
                                public void onNext(Comic comic) {
                                    comicsRepository.insertComic(comic);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    Log.d("Error", t.getMessage());
                                }

                                @Override
                                public void onComplete() {
                                    comicsRepository.saveTheLastComicId();
                                    Log.d("Complete", "Complete");
                                }
                            }));

    }

    @SuppressLint("CheckResult")
    public Maybe<Integer> CheckNumberOfRecordsInDb () { return comicsRepository.CheckNumberOfRecordsInDb(); }

    public Flowable<List<Comic>> getListToDisplayed(){return comicsRepository.getListComics(); }




}
