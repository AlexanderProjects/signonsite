package com.signonsite.test.comics.ui.shop;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import com.signonsite.test.comics.R;
import com.signonsite.test.comics.domain.helpers.Constants;
import com.signonsite.test.comics.ui.home.HomeComicsListFragment;

public class ShoppingFragment extends Fragment {

    public static ShoppingFragment newInstance() {
        return new ShoppingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shopping_fragment, container, false);
        ImageButton backNavigation = view.findViewById(R.id.back_nav_web);

        WebView web = view.findViewById(R.id.shoppingXkcd);
        web.loadUrl(Constants.xkcdMerchandiseUrl);

        backNavigation.setOnClickListener(viewNav ->
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, HomeComicsListFragment.newInstance())
                        .addToBackStack("Home")
                        .commitAllowingStateLoss());
        return  view;

    }

}
