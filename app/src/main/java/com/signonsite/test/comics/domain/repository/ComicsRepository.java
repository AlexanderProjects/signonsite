package com.signonsite.test.comics.domain.repository;

import com.bumptech.glide.RequestBuilder;
import com.signonsite.test.comics.domain.models.Comic;
import java.util.List;
import io.reactivex.Flowable;
import io.reactivex.Maybe;


public interface ComicsRepository {

    void insertComic(Comic comic);

    void saveTheLastComicId();

    void deleteComic(Comic comic);

    void updateComic(Comic comic);

    Flowable<Comic> getComic(String comicId);

    Flowable<List<Comic>> getListComics();

    Flowable<Comic> getLastComicIdPublished();

    Maybe<Integer> CheckNumberOfRecordsInDb();

    Maybe<String> getLastComicId();

    RequestBuilder getImage(String imgUrl);

}
