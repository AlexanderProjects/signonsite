package com.signonsite.test.comics.di.modules;

import com.signonsite.test.comics.di.modules.details.ComicDetailsViewModelModule;
import com.signonsite.test.comics.di.modules.home.HomeComicListViewModelModule;
import com.signonsite.test.comics.ui.details.ComicDetailsFragment;
import com.signonsite.test.comics.ui.home.HomeComicsListFragment;
import com.signonsite.test.comics.ui.home.HomeComicsListViewModel;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilderModule {

    @ContributesAndroidInjector(modules = {HomeComicListViewModelModule.class})
    public abstract HomeComicsListFragment contributeHomeFragment();

    @ContributesAndroidInjector(modules = {ComicDetailsViewModelModule.class})
    public abstract ComicDetailsFragment contributeDetailsFragment();
}
