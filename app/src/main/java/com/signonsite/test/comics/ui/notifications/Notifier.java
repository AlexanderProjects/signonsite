package com.signonsite.test.comics.ui.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;

import com.signonsite.test.comics.R;
import com.signonsite.test.comics.ui.home.ComicsMainActivity;

import java.util.Calendar;

public class Notifier {

    private ComicsMainActivity activity;

    public Notifier(ComicsMainActivity activity) {
        this.activity = activity;
    }

    public void execute() {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        Boolean dailyNotify = sharedPref.getBoolean("KEY_PREF_DAILY_NOTIFICATION", true);
        PackageManager pm = activity.getPackageManager();
        ComponentName receiver = new ComponentName(activity, DeviceBootReceiver.class);

        Intent alarmIntent = new Intent(activity, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);

        if (dailyNotify) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, sharedPref.getInt("dailyNotificationHour", 9));
            calendar.set(Calendar.MINUTE, sharedPref.getInt("dailyNotificationMin", 00));
            calendar.set(Calendar.SECOND, 1);

            if (calendar.before(Calendar.getInstance())) {
                calendar.add(Calendar.DATE, 1);
            }
            if (manager != null) {
                manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY, pendingIntent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }

            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        }else {
            if (PendingIntent.getBroadcast(activity, 0, alarmIntent, 0) != null && manager != null) {
                manager.cancel(pendingIntent);
            }
            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }

    }
}
