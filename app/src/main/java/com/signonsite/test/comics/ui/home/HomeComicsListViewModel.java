package com.signonsite.test.comics.ui.home;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.Display;

import androidx.lifecycle.ViewModel;

import com.signonsite.test.comics.domain.helpers.Constants;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.domain.usecases.HomeComicsListUseCase;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.inject.Inject;

import io.reactivex.MaybeObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeComicsListViewModel extends ViewModel {

    private HomeComicsListUseCase homeComicsListUseCase;
    private HomeComicsView homeComicsView;
    private Disposable disposableInitialDataObserver;
    private Disposable disposableDisplayedDataObserver;
    private List<Comic> comicsList = new ArrayList<>();


    @Inject
    public HomeComicsListViewModel(HomeComicsListUseCase homeComicsListUseCase) {
        this.homeComicsListUseCase = homeComicsListUseCase;
    }

    void inject(HomeComicsView homeComicsView) {
        this.homeComicsView = homeComicsView;
    }

    @SuppressLint("CheckResult")
    public void getListOfComics() {

        homeComicsListUseCase.CheckNumberOfRecordsInDb().subscribeOn(Schedulers.io())
                .subscribeWith(new MaybeObserver<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onSuccess(Integer recordsNumber) {
                        if(recordsNumber < Constants.threshold){
                            updateDataBase(recordsNumber);
                        }
                        displayDataUpdated();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Error : ", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        initialDataPopulation();
                        displayDataUpdated();
                    }
                });

    }


    @SuppressLint("CheckResult")
    private void initialDataPopulation(){
      disposableInitialDataObserver =  homeComicsListUseCase
                .getInitialListOfComics().subscribe();
    }

    @SuppressLint("CheckResult")
    private void displayDataUpdated(){
        disposableDisplayedDataObserver =  homeComicsListUseCase.getListToDisplayed()
                .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(comics -> {
                        comicsList = comics;
                        homeComicsView.setAdapter(comicsList);
                    });
    }

    @SuppressLint("CheckResult")
    private void updateDataBase(int listSize){
        homeComicsListUseCase.updateDataBase(listSize);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        if(disposableInitialDataObserver != null && disposableDisplayedDataObserver != null){
            disposableInitialDataObserver.dispose();
            disposableDisplayedDataObserver.dispose();
        }

    }
}
