package com.signonsite.test.comics.domain.usecases;

import com.bumptech.glide.RequestBuilder;
import com.signonsite.test.comics.domain.models.Comic;
import com.signonsite.test.comics.domain.repository.ComicsRepository;

import javax.inject.Inject;

public class ComicDetailsUseCase {
    private ComicsRepository comicsRepository;

    @Inject
    public ComicDetailsUseCase(ComicsRepository comicsRepository) {
        this.comicsRepository = comicsRepository;
    }

    public RequestBuilder processImage(String url){ return comicsRepository.getImage(url);}

    public void deleteComic(Comic comic){
        comicsRepository.deleteComic(comic);
    }

    public void saveComic(Comic comic){
        comicsRepository.updateComic(comic);
    }

}
